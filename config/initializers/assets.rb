# Be sure to restart your server when you modify this file.

# Version of your assets, change this if you want to expire all your assets.
Rails.application.config.assets.version = '1.0'

# Add additional assets to the asset load path
# Rails.application.config.assets.paths << Emoji.images_path

# Precompile additional assets.
# application.js, application.css, and all non-JS/CSS in app/assets folder are already added.
# Rails.application.config.assets.precompile += %w( search.js )
Rails.application.config.assets.precompile += %w(bootstrap/css/bootstrap.css )
Rails.application.config.assets.precompile += %w( index.css.erb )
Rails.application.config.assets.precompile += %w( video.css )
Rails.application.config.assets.precompile += %w( nav.css )
Rails.application.config.assets.precompile += %w( profile.css )
Rails.application.config.assets.precompile += %w( croppie/croppie.js )
Rails.application.config.assets.precompile += %w( croppie/croppie.css )
Rails.application.config.assets.precompile += %w( bootstrap/fonts/glyphicons-halflings-regular.eot )
Rails.application.config.assets.precompile += %w( bootstrap/fonts/glyphicons-halflings-regular.eot?#iefix )
Rails.application.config.assets.precompile += %w( bootstrap/fonts/glyphicons-halflings-regular.woff2 )
Rails.application.config.assets.precompile += %w( bootstrap/fonts/glyphicons-halflings-regular.woff )
Rails.application.config.assets.precompile += %w( bootstrap/fonts/glyphicons-halflings-regular.ttf )
Rails.application.config.assets.precompile += %w( bootstrap/fonts/glyphicons-halflings-regular.svg#glyphicons_halflingsregular )


