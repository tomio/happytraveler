Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
	resources :videos
	resources :users
    resources :countries 
    root 'videos#index'

    get 'video', to: "videos#video"
    get 'search', to: "videos#search"
    get 'login', to: "users#login"
    post 'login', to: "users#login"
    get 'logout', to: "users#logout"
    post 'new_view/:id', to: "videos#new_view"
    post '/new_comment', to: "videos#new_comment"
    post '/new_like', to: "videos#new_like"
    get 'add_flag', to: "videos#add_flag"
    post 'add_flag', to: "videos#post_flag"

end
