class AddThumbnailToVideo < ActiveRecord::Migration[5.0]
  def change
  	add_attachment :videos, :thumbnail 

  end
end
