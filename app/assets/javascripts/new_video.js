var editing = false; 
	var basic = $("#pic"); 
	var image_ok; 
	var video_ok; 


	function checkVideoFormat(fullPath)
	{
		filename = getUploadName(fullPath); 
		acceptable = ["mp4","mov"]; 
		var good = false; 
		extension = filename.substring(filename.indexOf('.') + 1); 
		for(i = 0; i < acceptable.length; i ++ )
		{
			good = acceptable[i] == extension; 
			if(good)
				break; 
		}

		return good; 

	}

	function checkImageFormat(fullPath)
	{
		filename = getUploadName(fullPath); 
		acceptable = ["jpg","jpeg","png"]; 
		var good = false; 
		extension = filename.substring(filename.indexOf('.') + 1); 
		for(i = 0; i < acceptable.length; i ++ )
		{
			good = acceptable[i] == extension; 
			if(good)
				break; 
		}

		return good; 

	}

	function submit()
	{
		 	
		var form = $("#form"); 
		if(!video_ok)
			alert("Please upload an acceptable video format"); 
		else if(!image_ok)
			alert("Please upload an acceptable image format"); 
		else 
			form.submit(); 

	}
function handleChange(event)
	{
		event.preventDefault();	
		var field = document.getElementById("uploadImage"); 
		var fullPath = field.value;
		image_ok = checkImageFormat(fullPath); 
		if (image_ok)
		{
			document.getElementById("pic").style.display = "block";
			files = event.target.files; 

			if(files && files[0])
				this.encode(files[0]); 
		}

		else 
			alert("Please upload a valid image")
	}


	function encode(file)
	{
		var reader = new FileReader();
		reader.onload = updateImageSource; 
		reader.readAsDataURL(file);
	}

	flip = function(way){
		rotate = -90; 
		if(way == 1)
			rotate = 90; 
		basic.croppie('rotate', rotate);	
	}

	updateImageSource = function (e) {

		if(!editing){
				basic.croppie({
		    viewport: { width: 300, height: 180,    type: 'square' },
		    boundary: { width: 300, height: 180 },
		    showZoomer: false,
		    enableOrientation: true,
		    enforceBoundary: true	
				});
				editing = true; 
		}


	buttons = document.getElementsByClassName("flip"); 
	buttons[0].style.display = "block";
	buttons[1].style.display = "block";
	result =  e.target.result; 

		basic.croppie('bind', {
   	 url: result 
		});


		
	}

	setVideoName = function(e)
	{

		var fullPath = document.getElementById("upload").value;

		var filename = getUploadName(fullPath); 
		video_ok = checkVideoFormat(filename); 	
		if(!video_ok)
			alert("Please upload an acceptable video format"); 
		document.getElementById("upload-path").innerHTML = filename ; 

	}

	getUploadName = function (fullPath) 
	{
		var filename = ""; 
		if (fullPath)
		{
	    var startIndex = (fullPath.indexOf('\\') >= 0 ? fullPath.lastIndexOf('\\') : fullPath.lastIndexOf('/'));
	    filename = fullPath.substring(startIndex);
	    if (filename.indexOf('\\') === 0 || filename.indexOf('/') === 0)
	    {
	    	filename = filename.substring(1);
	
	    } 
	    
		}

		return filename;

	}	


	