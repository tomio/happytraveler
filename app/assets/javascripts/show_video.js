


var view_counted = false; 

        $("#favorite-button").on("click",function()
        {   
            <% if not session[:user] == nil %> 
                var point = {}; 
                point ["video_id"] = <%= @video.id %>; 
                point ["user_id"] = <%= @user.id %> ;

                post("/new_like ",point,"post")
            <% else %> 
                window.location.replace("/users/new");

            <% end %>  

        }); 



    $("button").on("click",function()
    {
        $("#comment-form").submit();

    }); 


    videojs("my_video").ready(function(){
        myplayer = this;
        
        myplayer.on("play", function()
        {
            if(!view_counted)
            {
                post("/new_view/<%= @video.id %>","","post"); 
                view_counted = true; 
            }   

        });


    });


    function post(path, params, method) {
    method = method || "post"; // Set method to post by default if not specified.

    // The rest of this code assumes you are not using a library.
    // It can be made less wordy if you use one.
    var form = document.createElement("form");
    form.setAttribute("method", method);
    form.setAttribute("action", path);
    for(var key in params) {
        if(params.hasOwnProperty(key)) {
            var hiddenField = document.createElement("input");
            hiddenField.setAttribute("type", "hidden");
            hiddenField.setAttribute("name", key);
            hiddenField.setAttribute("value", params[key]);

            form.appendChild(hiddenField);
         }
    }

    document.body.appendChild(form);
    form.submit();
}