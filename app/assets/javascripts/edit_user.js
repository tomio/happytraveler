	var editing = false;  
	var basic = $('#img'); 
	var buttons = document.getElementsByClassName("pic-button"); 	
	var confirmButton = document.getElementsByClassName("confirm-button")[0]; 

	var result; 


function handleChange(event)
	{

		event.preventDefault();	
		//document.getElementById("img").style.display = "block";

		files = event.target.files; 

		if(files && files[0])
		{
			this.encode(files[0]); 
    }

	}

	function encode(file)
	{

		var reader = new FileReader();
		reader.onload = updateImageSource; 
		reader.readAsDataURL(file);

	}

	flip = function(way){
	rotate = -90; 
		if(way == 1)
			rotate = 90; 
		basic.croppie('rotate', rotate);

			
	}

	updateImageSource = function (e) {
		if(!editing){
					 basic.croppie({
		    viewport: { width: 150, height: 150 },
		    boundary: { width: 300, height: 300 },
		    showZoomer: true,
		    enableOrientation: true,
		    enforceBoundary: true	
				}	);
				editing = true; 
		}

		buttons[0].style.display = "initial";
		buttons[1].style.display = "initial";
		confirmButton.style.display = "initial"; 
		result =  e.target.result; 
		document.getElementById("orig").style.display = "none"; 

		basic.croppie('bind', {
   	 url: result 
		});

		

	}

	confirmImage = function()
	{

		basic.croppie('result', {
			type: 'base64', 
			size: 'viewport',
			circle: true 

		}).then(function(blob){
			document.getElementById("pic").value = blob; 
			var orig = document.getElementById("orig"); 
			orig.style.display = "initial"; 
			orig.setAttribute("src",blob); 
			buttons[0].style.display = "none";
			buttons[1].style.display = "none";
			confirmButton.style.display = "none"; 
			basic.croppie("destroy");
			editing = false;  


		}); 


	}

	submitform = function()
	{
		
		$("#form").submit();
	}

