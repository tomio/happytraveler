class Video < ApplicationRecord
	belongs_to :user
	has_many :comments
	has_many :likes

	has_attached_file :video,
	    processors: lambda { |a| a.is_video? ? [ :ffmpeg ] : [ :thumbnail ] }

	validates_attachment_content_type :video, content_type: /\Avideo\/.*\Z/

	has_attached_file :thumbnail, styles: {

		medium: "320x200>",
		small: "250x220>",
		large: "800x500>",
		thumb: "150x90!"

	}

	validates_attachment_content_type :thumbnail, content_type: /\image\/.*\Z/

	validates :title, presence: true	
	validates :country, presence: true	
	validates :city, presence: true	
	validates :video, presence: true
	validates :description, presence: true
	
	validates :thumbnail, presence: true



	def self.search(search)
	  if search
			where("title LIKE ?", "%#{search}%")
	  else
	  	Video.all
	  end
	end



end
