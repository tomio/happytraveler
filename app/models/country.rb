class Country < ApplicationRecord
	has_attached_file :flag,
	 :styles => {

		medium: "320x200>",
		small: "45x30>",
		large: "800x500>",
		thumb: "150x90!"

	}

	validates_attachment_content_type :flag, content_type: /\Aimage\/.*\Z/

end
