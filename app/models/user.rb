class User < ApplicationRecord
	has_secure_password
	has_many :videos
	has_many :comments
	has_many :likes

	has_attached_file :image,
	 :styles => {

		medium: "320x200>",
		small: "250x220>",
		large: "800x500>",
		thumb: "150x90!"

	},
 	 :default_url => "../../missing.png"
 	validates :email, presence: true	

	validates :password, presence: true	
	validates :name, presence: true	

	validates :password_confirmation, presence: false	
    validates :email, uniqueness: true
    validates_format_of :email, :with => /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i
	validates :password, length: { minimum: 6 }
	validates_attachment_content_type :image, content_type: /\Aimage\/.*\Z/

	

	def valid_attribute?(attribute_name)
  		self.valid?
  		self.errors[attribute_name].blank?
	end


end
