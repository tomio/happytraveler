class UsersController < ApplicationController
	def show 
		@user = User.find(params[:id])
		@boi = User.find(params[:id])
	end

	def new 
		@user = User.new 
	end

	def create 
		@user = User.new(user_params) 
		if @user.save
			session[:user] = @user.id
			redirect_to "/users/#{@user.id}"
		else 
			flash[:error] = "#{@user.errors.details}  "
			render action: :new 
		end 
	end 

	def edit 

		@user = User.find_by_id(params[:id])

		if params[:id].to_i == session[:user].to_i
			@user = User.find_by_id(params[:id])
		else 
			redirect_to "/videos"
		end 
	end 	

	def update 
		
		@user = User.find(params[:id])
		email = params[:user][:email]
		name = params[:user][:name]
		image = params[:user][:image]
		message = []
	

		if @user.email != email 	
			@user.email = email 
			if @user.valid_attribute?(:email)  

				@user.update_attribute(:email,email)
				message.push ("E-mail successfully updated")	
			else 
				message.push("E-mail is invalid")
				flash[:error] = @user.errors[:email]
				render action: :edit
				return

			end 
		end 

	if @user.name != name 	
			@user.name = name 
			if @user.valid_attribute?(:name)  

				@user.update_attribute(:name,name)
				message.push ("Name successfully updated")	
			else 
				message.push("Name is invalid")
				flash[:error] = @user.errors[:name]
				render action: :edit
				return

			end 

	end

		if @user.image != image 
			if @user.valid_attribute?(:image)  
				@user.update_attribute(:image, params[:user][:image])
				@image = image 
			else 
				flash[:error] = @user.errorss[:image]
				message.push("Image is invalid")
				render action: :edit
				return

			end 
		end 

		redirect_to "/users/"+@user.id.to_s






	end


	def logout 
		session[:user] = nil 
		redirect_to "/videos"

	end 

	def login
		@user = User.new  
		@error = false 
		if params.key?("email") and params.key?("password")
			@user = User.find_by(email: params[:email])
			if @user and @user.authenticate( params[:password])
				session[:user] = @user.id
			 redirect_to "/users/#{@user.id}"
			else 
				@user = User.new  
				@user.email = params[:email]
				@error = true 
				render action: :login
			end 	
		end 	
	end 

	private 

	def user_params
		params.require(:user).permit(:name,:email,:password,:image)

	end
end
