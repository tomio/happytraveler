	class VideosController < ApplicationController
	before_filter :get_user, :only => [:new,:create]
	skip_before_filter :verify_authenticity_token , :only => [:new_view,:new_comment,:new_like,:create]

	def index 
		@videos = Video.all

	end

	def video 
		@video = Video.all.last 
	end

	def show 
		@video = Video.find(params[:id])
		@videos	 = Video.all 	
		@comments = @video.comments 
		@liked = false 
		if not session[:user] == nil 
		 @user = User.find(session[:user])
		 @liked =  @video.likes.where(user_id: @user.id).first	 != nil 
		else 
			@user = nil 
		end 

	end 

	def new_like 

		video_id = params[:video_id]
		user_id = params[:user_id]

		like = Like.where(user_id: user_id, video_id: video_id).first	
		if like != nil 
			like.destroy()
			redirect_to "/videos/"+video_id.to_s
		else 
			like = Like.new 	
			like.video_id = video_id
			like.user_id = user_id
			
			if like.save 
				redirect_to "/videos/"+like.video_id.to_s
			end 
		end 
	end 

	def new_view 
		video =  Video.find(params[:id])
		video.update_attribute(:views, video.views+1)

	end 

	def new_comment 
		comment = Comment.new 
		comment.video_id = params[:video_id]
		comment.user_id = params[:user_id]
		comment.comment = params[:comment]

		if comment.save 
			redirect_to "/videos/"+comment.video_id.to_s
		end 

	end 

	def search 
		@query = params[:query]
		@videos = Video.search(@query)
		@pages = (@videos.count / 9.to_f).ceil

		@page = params[:page].to_i()	

		@start_index = 9 * (@page-1) 
		@end_index = @start_index + 8

		if @start_index > @videos.count
			@start_index = 0
			@end_index = 8 
		end
		
		if @end_index > @videos.count - 1
			 @end_index = @videos.count - 1 
		end

	end

	def add_flag 
		@country = Country.new 
	end 

	def post_flag
		@country = Country.new(country_params)
		if @country.save
			redirect_to "/videos"
		else 
			render action: :add_flag 
		end 
	end 

	def new 
		@messages = []
		@countries = Country.all
		@video = Video.new 
	end 

	def create 
		@video = @user.videos.new(video_params)
		if @video.save
			redirect_to "/videos"
		else 

			@messages = @video.errors 
			render action: :new 
		end 
	end 

end

private 

def video_params 
	params.require(:video).permit(:title,:video,:thumbnail,:city,:country,:description)
end 

def country_params 
	params.require(:country).permit(:name,:flag)
end

def get_user 
	if not session[:user] 
		redirect_to "/users/new"
	else 
		@user = User.find(session[:user])

	end 

end 


